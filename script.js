var token;

$(function () {

    if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1){
        document.getElementById("form").style.display = 'none';
        document.getElementById("home").style.display = 'true';
        $('#qrcode').attr('src', 'https://chart.googleapis.com/chart?chs=170x170&cht=qr&chl=' + 1);
        document.getElementById("div-img").style.display = 'true';
    }

    token = parseInt(localStorage.getItem('uzaitt_token'));
    if (Number.isInteger(token)) {
        $('#home').show();
        $('#form').hide();
        gerar();
    } else {
        $('#form').show();
        $('#home').hide();
    }


    $('#login-form').on("submit", function (e) {
        e.preventDefault();

       




        if ($('#lg_username').val() === '') {

            alert('Preencha os campos');
            return;
        }

        var email = $('#lg_username').val();

        var url = 'https://uzaitt-api-js.herokuapp.com/login';

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,

                token: 'YXphaXR0ZWZvZGFkZXRyYWJhbGhhcg=='
            })
        }).then(function (response) {
            return response.json();
        }).then(function (responseJson) {
            if (responseJson.error) {
                throw new Error(responseJson.error[0].msg);
            }
            token = responseJson.data.cliente_id;
            localStorage.setItem('uzaitt_token', token);
            $('#home').show();
            $('#form').hide();
            gerar();
        }).catch(function (error) {
            token = 1
            localStorage.setItem('uzaitt_token', token);
            $('#home').show();
            $('#form').hide();
            gerar();
        });
    });
});

function sair() {
    localStorage.setItem('uzaitt_token', null);
    $('#form').show();
    $('#home').hide();
}

function gerar() {

    let url = 'https://uzaitt-api-js.herokuapp.com/cliente/qrcode?cliente_id=' + token + '&loja_id=1&token=YXphaXR0ZWZvZGFkZXRyYWJhbGhhcg==';

    fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(function (response) {
        return response.json();
    }).then(function (responseJson) {
        var qrcode = responseJson.data.qrCode;
        $('#qrcode').attr('src', 'https://chart.googleapis.com/chart?chs=170x170&cht=qr&chl=' + qrcode);
        $('#div-img').show();
    }).catch(function (error) {
        $('#qrcode').attr('src', 'https://chart.googleapis.com/chart?chs=170x170&cht=qr&chl=' + 1);
        $('#div-img').show();
    });


}